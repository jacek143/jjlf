from configuration import Configuration
from camera import Camera
from image_analyzer import ImageAnalyzer
from speed_controller import SpeedController as SpeedController
from remote_control import RemoteControl
from bluedot import BlueDot
import numpy as np
from motor_driver import MotorDriver


remote_start_stop = RemoteControl(Configuration, BlueDot())
analyzer = ImageAnalyzer(Configuration)
speed_controller = SpeedController(Configuration)
with Camera(Configuration) as camera, MotorDriver(Configuration) as motors:
    try:
        while True:
            img = camera.process()
            print('{} {};'.format(np.max(img), np.min(img)), end=' ')
            error = analyzer.process(img)
            print('{};'.format(error), end=' ')
            speed = speed_controller.process(error)
            print('{};'.format(speed), end=' ')
            supervised_speed = remote_start_stop.process(speed)
            print('{};'.format(supervised_speed), end=' ')
            motors.process(supervised_speed)
            print()
    except KeyboardInterrupt:
        pass
