from picamera import PiCamera
from picamera.array import PiYUVArray
from time import sleep


class Camera:
    def __init__(self, configuration):
        self.__picamera = PiCamera(resolution=(configuration['image width'], configuration['image height']), framerate=configuration['camera framerate'])
        sleep(2)  # warmup
        self.__pioutput = PiYUVArray(self.__picamera)

    def process(self):
        self.__pioutput.truncate(0)
        self.__picamera.capture(self.__pioutput, 'yuv', use_video_port=True)
        return self.__pioutput.array[:,:,0]

    def close(self):
        self.__picamera.close()

    def __enter__(self):
        return self
        
    def __exit__(self, *args):
        self.__picamera.close()


if __name__ == '__main__':
    from time import time
    from configuration import Configuration
    with Camera(Configuration) as sensor:
        time_start = time()
        image_array = sensor.process()
        read_duration = time() - time_start

    print(u'\u0394'+'t='+str(read_duration))
    from PIL import Image
    image = Image.fromarray(image_array)
    image.show()
