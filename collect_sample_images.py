from camera import Camera
from PIL import Image
from time import time
from numpy import save
from math import isnan
from configuration import Configuration


def is_label_correct(label_string):
    try:
        numeric_value = float(label_string)
        return -1.0 <= numeric_value <= 1.0 or isnan(numeric_value)
    except ValueError:
        return False


try:
    with Camera(Configuration) as sensor:
        while True:
            raw_image = sensor.process()
            Image.fromarray(raw_image).show()
            label = input('label: ')
            if is_label_correct(label):
                file_name = 'sample_images/' + str(time()).replace('.', '') + '_' + label
                save(file_name, raw_image)
                print('saved as ' + file_name)
            else:
                print('skipped')


except KeyboardInterrupt:
    pass
