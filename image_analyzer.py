import numpy as np
from collections import namedtuple
import math

Sensor = namedtuple('Sensor', ['row', 'column', 'weight'])


class ImageAnalyzer:
    def __init__(self, configuration):
        self.__threshold = configuration['image threshold']
        self.__columns_count = configuration['image width']
        self.__rows_count = configuration['image height']
        self.__sensors = self.__construct_sensors()

    def process(self, image):
        weights_of_activated = [sensor.weight for sensor in self.__sensors if
                                image[sensor.row, sensor.column] < self.__threshold]
        return np.mean(weights_of_activated) if len(weights_of_activated) != 0 else np.nan

    def __construct_sensors(self):
        sensors = list()
        for column in range(0, self.__columns_count):
            weight = 2 * column / (self.__columns_count - 1) - 1
            row = int((self.__rows_count - 1) * abs(weight))
            sensors.append(Sensor(row, column, weight))
        return sensors
