import numpy as np


class SpeedController:

    def __init__(self, configuration):
        self.__STRAIGHT_PWM = configuration['speed controller straight pwm']
        self.__THRESHOLD = configuration['speed controller error threshold']
        self.__TURN_PWM = configuration['speed controller turn pwm']
        self.__speed = (self.__STRAIGHT_PWM, self.__STRAIGHT_PWM)
        
    def process(self, error):
        if np.isnan(error):
            pass
        else:
            turn_pwm = np.sign(error) * self.__TURN_PWM
            straight_pwm = self.__STRAIGHT_PWM
            if abs(error) < self.__THRESHOLD:
                self.__speed = straight_pwm, straight_pwm
            else:
                self.__speed = turn_pwm, -turn_pwm
        return self.__speed
