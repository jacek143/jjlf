import unittest
from image_analyzer import ImageAnalyzer
import os
import numpy as np
import re
from math import isnan
from configuration import Configuration


# @unittest.skip("Those tests are expensive to run")
class TestImageAnalyzer(unittest.TestCase):
    def setUp(self):
        self.analyzer = ImageAnalyzer(Configuration)

    def test_real_images(self):
        samples_directory = os.getcwd() + '/sample_images'
        for file_name in os.listdir(samples_directory):
            expected = float(re.findall('.*_(.*).npy', file_name)[0])
            actual = self.analyzer.process(np.load(samples_directory + '/' + file_name))
            self.assertValidOutput(expected, actual)

    def assertValidOutput(self, expected, actual):
        if isnan(expected):
            self.assertTrue(isnan(actual))
        else:
            self.assertAlmostEqual(expected, actual, delta=0.1)
