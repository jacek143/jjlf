def compute(sensors):
    detected = [i*sensors[i] for i in range(len(sensors)) if sensors[i]]
    if len(detected) > 0:
        average = sum(detected)/len(detected)
        normalized_zero_to_one = average / (len(sensors)-1)
        error = 2*normalized_zero_to_one - 1
    else:
        error = None
    return error

    