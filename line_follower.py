from configuration import Configuration
from camera import Camera
from image_analyzer import ImageAnalyzer
from speed_controller import SpeedController as SpeedController
from remote_control import RemoteControl
from bluedot import BlueDot
from motor_driver import MotorDriver

try:
    blue_dot = BlueDot()
    image_analyzer = ImageAnalyzer(Configuration)
    speed_controller = SpeedController(Configuration)
    remote_controller = RemoteControl(Configuration, blue_dot)
    with Camera(Configuration) as camera, MotorDriver(Configuration) as motors:
        while True:
            img = camera.process()
            error = image_analyzer.process(img)
            auto_speed = speed_controller.process(error)
            supervised_speed = remote_controller.process(auto_speed)
            motors.process(supervised_speed)
except KeyboardInterrupt:
    pass
