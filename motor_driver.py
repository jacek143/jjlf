from motor import Motor
from gpiozero import DigitalOutputDevice, DigitalInputDevice, PWMOutputDevice


class MotorDriver:
    def __init__(self, configuration):
        frequency = configuration['motors pwm frequency']

        self.__left_pwm = PWMOutputDevice(configuration['left motor pwm pin'], frequency=frequency)
        self.__left_in1 = DigitalOutputDevice(configuration['left motor in1 pin'])
        self.__left_in2 = DigitalOutputDevice(configuration['left motor in2 pin'])
        self.__left_motor = Motor(self.__left_pwm, self.__left_in1, self.__left_in2)

        self.__right_pwm = PWMOutputDevice(configuration['right motor pwm pin'], frequency=frequency)
        self.__right_in1 = DigitalOutputDevice(configuration['right motor in1 pin'])
        self.__right_in2 = DigitalOutputDevice(configuration['right motor in2 pin'])
        self.__right_motor = Motor(self.__right_pwm, self.__right_in1, self.__right_in2)

    def process(self, speed):
        self.__left_motor.value = speed[0]
        self.__right_motor.value = speed[1]

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.__left_pwm.close()
        self.__left_in1.close()
        self.__left_in2.close()
        self.__right_pwm.close()
        self.__right_in1.close()
        self.__right_in2.close()
