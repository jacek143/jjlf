# -*- coding: utf-8 -*-

class Motor:

    def __init__(self, pwm, in1, in2):
        self._pwm = pwm
        self._in1 = in1
        self._in2 = in2
        self._value = 0

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, new_value):
        self._value = new_value
        self._pwm.value = abs(new_value)
        is_positive = new_value > 0
        self._in1.value = is_positive
        self._in2.value = not is_positive