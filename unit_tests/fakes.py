class PWMOutputDeviceFake:
    def __init__(self):
        self.value = 0


class DigitalOutputDeviceFake:
    def __init__(self):
        self.value = False


class BlueDotFake:
    def __init__(self):
        self.when_pressed = None
        self.when_released = None


class BlueDotPositionFake:
    def __init__(self):
        self.top = False
        self.left = False
        self.right = False
        self.bottom = False
        self.middle = False
