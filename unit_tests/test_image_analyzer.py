import unittest
from image_analyzer import ImageAnalyzer
import numpy as np
from math import isnan


class TestImageAnalyzer(unittest.TestCase):
    def setUp(self):
        self.threshold = 50
        self.width = 24
        self.height = 12
        self.analyzer = ImageAnalyzer({
            'image width': self.width,
            'image height': self.height,
            'image threshold': self.threshold
        })
        self.raw_image = np.ones((self.height, self.width), dtype=np.uint8) * 255

    def assertValue(self, expected):
        actual = self.analyzer.process(self.raw_image)
        if isnan(expected):
            self.assertTrue(isnan(actual))
        else:
            self.assertAlmostEqual(expected, actual)

    def given_grey_pixel(self, coordinates, value):
        self.raw_image[coordinates] = value


class TestImageAnalyzerSensingPoints(TestImageAnalyzer):
    def given_black_pixel(self, coordinates):
        self.given_grey_pixel(coordinates, 0)

    def test_no_line(self):
        self.assertValue(float('nan'))

    def test_middle(self):
        # we have even number of columns so 2 sensing points are in the middle
        self.given_black_pixel((0, int(self.width / 2)-1))
        self.given_black_pixel((0, int(self.width / 2)))
        self.assertValue(0)

    def test_extreme_left(self):
        self.given_black_pixel((-1, 0))
        self.assertValue(-1)

    def test_extreme_right(self):
        self.given_black_pixel((-1, -1))
        self.assertValue(1)


class TestImageAnalyzerThreshold(TestImageAnalyzer):
    def test_above(self):
        self.given_grey_pixel((-1, 0), self.threshold+1)
        self.assertValue(float('nan'))

    def test_equal(self):
        self.given_grey_pixel((-1, 0), self.threshold)
        self.assertValue(float('nan'))

    def test_below(self):
        self.given_grey_pixel((-1, 0), self.threshold-1)
        self.assertValue(-1)

