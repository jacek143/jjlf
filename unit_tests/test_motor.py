# -*- coding: utf-8 -*-
import unittest
from unit_tests.fakes import PWMOutputDeviceFake, DigitalOutputDeviceFake
from motor import Motor


class TestMotorConstruction(unittest.TestCase):
    
    def setUp(self):
        self.pwm = PWMOutputDeviceFake()
        self.in1 = DigitalOutputDeviceFake()
        self.in2 = DigitalOutputDeviceFake()
        self.motor = Motor(self.pwm, self.in1, self.in2)
    
    def test_construction(self):
        self.assertEqual(self.pwm.value, 0)
        self.assertEqual(self.in1.value, False)
        self.assertEqual(self.in2.value, False)
    
    def test_stop(self):
        self.motor.value = 0
        self.assertEqual(self.pwm.value, 0)
    
    def test_positive_pwm(self):
        self.motor.value = 0.7
        self.assertAlmostEqual(self.pwm.value, 0.7)
    
    def test_negative_pwm(self):
        self.motor.value = -0.32
        self.assertAlmostEqual(self.pwm.value, 0.32)
    
    def test_forward(self):
        self.motor.value = 0.6
        self.assertTrue(self.in1.value)
        self.assertFalse(self.in2.value)
    
    def test_backward(self):
        self.motor.value = -0.3
        self.assertFalse(self.in1.value)
        self.assertTrue(self.in2.value)
    
    def test_stored_value(self):
        self.motor.value = 0.47
        self.assertAlmostEqual(self.motor.value, 0.47)