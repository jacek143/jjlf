import unittest
from remote_control import RemoteControl
from unit_tests.fakes import BlueDotFake
from unit_tests.fakes import BlueDotPositionFake


class TestRemoteStartStop(unittest.TestCase):
    def setUp(self):
        self.pwm = 0.5
        self.bluetooth_fake = BlueDotFake()
        self.remote = RemoteControl({'remote control pwm': self.pwm}, self.bluetooth_fake)

    def press(self, position_string):
        position = BlueDotPositionFake()
        if position_string == 'middle':
            position.middle = True
        elif position_string == 'top':
            position.top = True
        elif position_string == 'left':
            position.left = True
        elif position_string == 'right':
            position.right = True
        elif position_string == 'bottom':
            position.bottom = True
        self.bluetooth_fake.when_pressed(position)

    def release(self):
        self.bluetooth_fake.when_released()

    def assertEqualSpeeds(self, expected, actual):
        self.assertAlmostEqual(expected[0], actual[0])
        self.assertAlmostEqual(expected[1], actual[1])

    def test_no_touching(self):
        self.assertEqualSpeeds((0, 0), self.remote.process((0.32, 0.14)))

    def test_start(self):
        self.press('middle')
        speed = (0.40, 0.29)
        self.assertEqualSpeeds(speed, self.remote.process(speed))

    def test_stop_middle(self):
        self.press('middle')
        self.press('middle')
        self.assertEqualSpeeds((0, 0), self.remote.process((0.56, 0.21)))

    def test_stop_whatever(self):
        self.press('middle')
        self.press('left')
        self.assertEqualSpeeds((0, 0), self.remote.process((0.45, -0.54)))

    def test_remote_top(self):
        self.press('top')
        self.assertEqualSpeeds((self.pwm, self.pwm), self.remote.process((-0.34, 0.64)))
        self.release()
        self.assertEqualSpeeds((0, 0), self.remote.process((-0.96, 0.439)))

    def test_remote_left(self):
        self.press('left')
        self.assertEqualSpeeds((-self.pwm, self.pwm), self.remote.process((0.64, 0.34)))
        self.release()
        self.assertEqualSpeeds((0, 0), self.remote.process((0.53, 0.98)))

    def test_remote_right(self):
        self.press('right')
        self.assertEqualSpeeds((self.pwm, -self.pwm), self.remote.process((-0.95, 0.29)))
        self.release()
        self.assertEqualSpeeds((0, 0), self.remote.process((0.37, 0.31)))

    def test_remote_bottom(self):
        self.press('bottom')
        self.assertEqualSpeeds((-self.pwm, -self.pwm), self.remote.process((0.49, -0.54)))
        self.release()
        self.assertEqualSpeeds((0, 0), self.remote.process((0.69, 0.39)))
