import unittest
import error_calculator


class TestErrorCalculator(unittest.TestCase):
    def test_line_on_the_middle(self):
        sensors = [False, True, False]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(0, error)

    def test_line_slightly_on_the_left(self):
        sensors = [False, True, False, False, False]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(-0.5, error)
        
    def test_no_line(self):
        sensors = [False, False, False, False]
        error = error_calculator.compute(sensors)
        self.assertIsNone(error)
    
    def test_line_on_the_far_right(self):
        sensors = [False, False, False, True]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(1.0, error)
    
    def test_line_on_the_far_left(self):
        sensors = [True, False, False, False, False]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(-1.0, error)
    
    def test_line_detected_by_many_sensors(self):
        sensors = [True, True, False, False, False]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(-0.75, error)

    # TODO: I don't know how robot should behave in this situation    
    def test_all_sensors_detected(self):
        sensors = [True, True, True, True, True, True]
        error = error_calculator.compute(sensors)
        self.assertAlmostEqual(0, error)
