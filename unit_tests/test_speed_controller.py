from speed_controller import SpeedController
import unittest

STRAIGHT_PWM = 0.3
TURN_PWM = 0.15
ERROR_THRESHOLD = 0.2


class TestSpeedController(unittest.TestCase):

    def test_no_error(self):
        self.given_error(0)
        self.assert_speed(STRAIGHT_PWM, STRAIGHT_PWM)

    def test_error_below_threshold(self):
        self.given_error(ERROR_THRESHOLD - 0.01)
        self.assert_speed(STRAIGHT_PWM, STRAIGHT_PWM)

    def test_error_above_threshold(self):
        self.given_error(ERROR_THRESHOLD + 0.01)
        self.assert_speed(TURN_PWM, -TURN_PWM)

    def test_negative_error(self):
        self.given_error(-ERROR_THRESHOLD - 0.01)
        self.assert_speed(-TURN_PWM, TURN_PWM)

    def test_nan_error_ignored(self):
        self.given_error(ERROR_THRESHOLD + 0.3)
        self.assert_speed(TURN_PWM, -TURN_PWM)
        self.given_error(float('nan'))
        self.assert_speed(TURN_PWM, -TURN_PWM)
        
    def setUp(self):
        configuration = {
            'speed controller straight pwm': STRAIGHT_PWM,
            'speed controller turn pwm': TURN_PWM,
            'speed controller error threshold': ERROR_THRESHOLD,
        }
        self.controller = SpeedController(configuration)

    def given_error(self, error):
        self.error = error

    def assert_speed(self, expected_left, expected_right):
        speed = self.controller.process(self.error)
        self.assertAlmostEqual(expected_left, speed[0])
        self.assertAlmostEqual(expected_right, speed[1])
