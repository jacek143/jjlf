from enum import Enum


class State(Enum):
    STOP = 0
    AUTO = 1
    REMOTE = 2


class RemoteControl:
    
    def __init__(self, configuration, blue_dot):
        self.__pwm = configuration['remote control pwm']
        blue_dot.when_pressed = self.__when_pressed
        blue_dot.when_released = self.__when_released
        self.__last_pressed_position = None
        self.__state = State.STOP

    def __when_pressed(self, position):
        if self.__state == State.STOP:
            if position.middle:
                self.__state = State.AUTO
            else:
                self.__state = State.REMOTE
        elif self.__state == State.AUTO:
            self.__state = State.STOP

        self.__last_pressed_position = position

    # I don't care about position in this case
    def __when_released(self, position=None):
        if self.__state == State.REMOTE:
            self.__state = State.STOP

    def process(self, auto_speed):
        if self.__state == State.AUTO:
            return auto_speed
        elif self.__state == State.REMOTE:
            if self.__last_pressed_position.top:
                return self.__pwm, self.__pwm
            elif self.__last_pressed_position.left:
                return -self.__pwm, self.__pwm
            elif self.__last_pressed_position.right:
                return self.__pwm, -self.__pwm
            elif self.__last_pressed_position.bottom:
                return -self.__pwm, -self.__pwm
        return 0, 0

